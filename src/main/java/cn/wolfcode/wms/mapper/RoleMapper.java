package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.Role;
import cn.wolfcode.wms.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Role entity);

    Role selectByPrimaryKey(Long id);

    List<Role> selectAll();

    int updateByPrimaryKey(Role entity);

    Long queryForCount(QueryObject qo);

    List<Role> queryForList(QueryObject qo);

    void saveRolePermissionRelations(@Param("roleId") Long roleId, @Param("pemissionId") Long pemissionId);

    void deleteRolePermissionRelations(Long id);

    /**
     * 根据员工的id来查询角色
     * @param empId
     * @return
     */
    List<Role> selectRoleByEmployeeId(Long empId);

    void saveRoleSystemMenuRelations(@Param("roleId") Long roleId, @Param("systemMenuId") Long systemMenuId);

    void deleteRoleSystemMenuRelations(Long roleId);
}