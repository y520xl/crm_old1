package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.SystemMenu;
import cn.wolfcode.wms.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SystemMenuMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemMenu entity);

    SystemMenu selectByPrimaryKey(Long id);

    List<SystemMenu> selectAll();

    int updateByPrimaryKey(SystemMenu entity);

    Long queryForCount(QueryObject qo);

    List<SystemMenu> queryForList(QueryObject qo);

    /**
     * 查询所有的子级菜单
     * @param parentSn
     * @return
     */
    List<Map<String,Object>> selectMenuByParentSn(String parentSn);

    List<Map<String,Object>> selectMenuByParentSnAndEmpId(@Param("parentSn") String parentSn, @Param("empId") Long empId);
}