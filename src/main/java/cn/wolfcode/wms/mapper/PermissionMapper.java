package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.Permission;
import cn.wolfcode.wms.query.QueryObject;
import java.util.List;

public interface PermissionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Permission entity);

    List<Permission> selectAll();

    Long queryForCount(QueryObject qo);

    List<Permission> queryForList(QueryObject qo);

    List<String> getExpression();

    List<Permission> selectPermissionByRoleId();

    List<String> selectPermissionsByEmployeeId(Long empId);
}