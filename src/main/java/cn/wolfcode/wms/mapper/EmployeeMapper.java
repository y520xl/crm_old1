package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.Employee;
import cn.wolfcode.wms.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Employee entity);

    Employee selectByPrimaryKey(Long id);

    List<Employee> selectAll();

    int updateByPrimaryKey(Employee entity);


    Long queryForCount(QueryObject qo);

    List<Employee> queryForList(QueryObject qo);

    /**
     * 批量删除
     * @param ids
     */
    void batchDelete(Long[] ids);

    /**
     * 保存员工和角色之间的关系
     * @param empId
     * @param roleId
     */
    void saveEmployeeRoleRelations(@Param("empId") Long empId, @Param("roleId") Long roleId);

    /**
     * 删除员工和角色之间的旧关系
     * @param empId
     */
    void deleteEmployeeRoleRelation(Long empId);

    Employee login(@Param("name") String name, @Param("password") String password);
}