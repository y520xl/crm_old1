package cn.wolfcode.wms.mapper;

import cn.wolfcode.wms.domain.ProductStock;
import cn.wolfcode.wms.query.ProductStockQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductStockMapper {

    int insert(ProductStock entity);

    int updateByPrimaryKey(ProductStock entity);

    ProductStock selectByProductIdAndDepotId(@Param("productId") Long productId, @Param("depotId") Long depotId);

    Long queryForCount(ProductStockQueryObject qo);

    List<ProductStock> queryForList(ProductStockQueryObject qo);

}