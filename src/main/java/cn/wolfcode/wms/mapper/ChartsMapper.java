package cn.wolfcode.wms.mapper;


import cn.wolfcode.wms.query.OrderChartsQueryObject;
import cn.wolfcode.wms.query.SaleChartsQueryObject;

import java.util.List;
import java.util.Map;

public interface ChartsMapper {

    List<Map<String,Object>> selectOrderCharts(OrderChartsQueryObject qo);

    List<Map<String,Object>> selectSaleCharts(SaleChartsQueryObject qo);
}
