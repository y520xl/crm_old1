package cn.wolfcode.wms.utils;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class JsonResult {

    private boolean success = true;
    private String message;

    public void mark(String message){
        this.success = false;
        this.message = message;
    }
}
