package cn.wolfcode.wms.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Collections;
import java.util.List;

@Setter@Getter@ToString
public class PageResult {

    private List<?> listData;
    private Long  totalCount;

    private Long currentPage;
    private Long pageSize;

    private Long totalPage;
    private Long nextPage;
    private Long prevPage;

    public PageResult(List<?> listData, Long totalCount, Long currentPage, Long pageSize) {
        this.listData = listData;
        this.totalCount = totalCount;
        this.currentPage = currentPage;
        this.pageSize = pageSize;

        if (totalCount < pageSize){
            totalPage = 1L;
            nextPage = 1L;
            prevPage = 1L;

            return ;
        }

        totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount/ pageSize + 1;
        prevPage = currentPage - 1 > 0 ?  currentPage - 1 : 1;
        nextPage = currentPage + 1 < totalPage ? currentPage + 1: totalPage;
    }

    public PageResult(Long pageSize){

        this(Collections.EMPTY_LIST,0L,1L,pageSize);
    }
}
