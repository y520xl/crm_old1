package cn.wolfcode.wms.utils;

import java.lang.reflect.Method;

public class PermissionUtil {

    private PermissionUtil(){}

    public static String getExpression(Method method){

        String className = method.getDeclaringClass().getName();
        String methodName = method.getName();
        String expression = className +":"+ methodName;

        return expression;
    }
}
