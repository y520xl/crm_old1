package cn.wolfcode.wms.utils;

import cn.wolfcode.wms.domain.Employee;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

public class UserContext {

    private UserContext(){};
    public static HttpSession getSession(){

        return ((ServletRequestAttributes)(RequestContextHolder.getRequestAttributes()))
                .getRequest().getSession();
    }

    public static void setEmployee(Employee curent){
        getSession().setAttribute("EMPLOYEE_IN_SESSION",curent);
    }
    public static Employee getEmployee(){
        return (Employee) getSession().getAttribute("EMPLOYEE_IN_SESSION");
    }

    public static void setExpression(List<String> expressions){

        getSession().setAttribute("EXPRESSION_IN_SESSION",expressions);
    }
    public static List<String> getExpression(){
        return (List<String>) getSession().getAttribute("EXPRESSION_IN_SESSION");
    }
}
