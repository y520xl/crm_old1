package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Depot;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IDepotService;
import cn.wolfcode.wms.utils.JsonResult;
import cn.wolfcode.wms.utils.RequirePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("depot")
public class DepotController {

	@Autowired
	private IDepotService service;

	@RequestMapping("list")
	@RequirePermission("仓库列表")
	public String list(Model model ,@ModelAttribute("qo")QueryObject qo) {

		model.addAttribute("result", service.query(qo));
		return "depot/list";
	}

	@RequestMapping("delete")
	@RequirePermission("仓库删除")
	@ResponseBody
	public JsonResult delete(Long id) {
		JsonResult result = new JsonResult();
		try{
			if(id != null){
				service.deleteByPrimaryKey(id);
			}

		}catch (Exception e){
			e.printStackTrace();
			result.mark("删除失败!");
		}

		return result;
	}
	@RequestMapping("input")
	@RequirePermission("仓库编辑")
	public String input(Model model, Long id) {

		if (id != null) {
			model.addAttribute("depot",service.selectByPrimaryKey(id));
		}
		return "depot/input";
	}

	@RequestMapping("saveOrUpdate")
	@RequirePermission("仓库保存或更新")
	@ResponseBody
	public JsonResult saveOrUpdate(Depot depot){
		JsonResult result = new JsonResult();
		try{
			if(depot.getId()!=null){
				service.updateByPrimaryKey(depot);

			}else {
				service.insert(depot);
			}
		}catch (Exception e){
			e.printStackTrace();
			result.mark("保存或更新失败");
		}
		return result;
	}

}
