package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Product;
import cn.wolfcode.wms.query.ProductQueryObject;
import cn.wolfcode.wms.service.IBrandService;
import cn.wolfcode.wms.service.IProductService;
import cn.wolfcode.wms.utils.JsonResult;
import cn.wolfcode.wms.utils.RequirePermission;
import cn.wolfcode.wms.utils.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;

@Controller
@RequestMapping("product")
public class ProductController {

    @Autowired
    private IProductService service;
    @Autowired
    private IBrandService brandService;
    @Autowired
    private ServletContext ctx;
    @RequestMapping("list")
    @RequirePermission("商品列表")
    public String list(Model model, @ModelAttribute("qo") ProductQueryObject qo){

        model.addAttribute("result", service.query(qo));
        model.addAttribute("brands",brandService.selectAll());
        return "product/list";
    }

    @RequestMapping("delete")
    @ResponseBody
    @RequirePermission("商品删除")
    public JsonResult delete(Long id,String imagePath){
        JsonResult result = new JsonResult();
        try{
            if(id != null){
                UploadUtil.deleteFile(ctx,imagePath);
                service.deleteByPrimaryKey(id);
            }

        }catch (Exception e){
            e.printStackTrace();
            result.mark("删除失败!");
        }

        return result;
    }

    @RequestMapping("input")
    @RequirePermission("商品编辑")
    public String input(Long id,Model model){

        model.addAttribute("brands",brandService.selectAll());
        if(id!=null){
            Product product = service.selectByPrimaryKey(id);
            model.addAttribute("product",product);
        }

        return "product/input";
    }
    @RequestMapping("saveOrUpdate")
    @RequirePermission("商品保存或更新")
    @ResponseBody
    public JsonResult saveOrUpdate(Product product, MultipartFile pic){

        System.out.println(pic);
        if (pic!=null && StringUtils.hasLength(product.getImagePath())){
            //删除图片
            UploadUtil.deleteFile(ctx,product.getImagePath());

        }
        if (pic!=null){

            //保存图片
            String imagePath = UploadUtil.upload(pic, ctx.getRealPath("/upload"));
            product.setImagePath(imagePath);

        }

        JsonResult result = new JsonResult();

        try{
            if(product.getId()!=null){
                service.updateByPrimaryKey(product);

            }else {
                service.insert(product);
            }
        }catch (Exception e){
            e.printStackTrace();
            result.mark("保存或更新失败");
        }
        return result;
    }

    @RequestMapping("selectProductList")
    public String selectProductList(Model model, @ModelAttribute("qo") ProductQueryObject qo){

        model.addAttribute("result", service.query(qo));
        model.addAttribute("brands",brandService.selectAll());
        return "product/selectProductList";
    }
}
