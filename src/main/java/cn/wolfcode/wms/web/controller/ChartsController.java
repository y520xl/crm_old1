package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.query.OrderChartsQueryObject;
import cn.wolfcode.wms.query.SaleChartsQueryObject;
import cn.wolfcode.wms.service.IBrandService;
import cn.wolfcode.wms.service.IChartsService;
import cn.wolfcode.wms.service.IClientService;
import cn.wolfcode.wms.service.ISupplierService;
import cn.wolfcode.wms.utils.RequirePermission;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("charts")
public class ChartsController {

    @Autowired
    private IChartsService service;

    @Autowired
    private ISupplierService supplierService;
    @Autowired
    private IBrandService brandService;

    @Autowired
    private IClientService clientService;
    @RequestMapping("orderCharts")
    @RequirePermission("订单报表列表")
    public String orderCharts(Model model, @ModelAttribute("qo")OrderChartsQueryObject qo){

        model.addAttribute("result", service.selectOrderCharts(qo));
        model.addAttribute("suppliers", supplierService.selectAll());
        model.addAttribute("brands", brandService.selectAll());
        model.addAttribute("groupByTypes", OrderChartsQueryObject.groupByTypes);
        return "charts/orderCharts";
    }
    @RequestMapping("saleCharts")
    @RequirePermission("销售报表列表")
    public String saleCharts(Model model, @ModelAttribute("qo")SaleChartsQueryObject qo){

        model.addAttribute("result", service.selectSaleCharts(qo));
        model.addAttribute("clients", clientService.selectAll());
        model.addAttribute("brands", brandService.selectAll());
        model.addAttribute("groupByTypes", SaleChartsQueryObject.groupByTypes);
        return "charts/saleCharts";
    }
    @RequestMapping("sale_bar")
    @RequirePermission("柱状图")
    public String saleChartsByBar(Model model, @ModelAttribute("qo")SaleChartsQueryObject qo){

        List<Map<String, Object>> list  = service.selectSaleCharts(qo);
        //存储分组类型的所有数据
        List<Object> groupByType = new ArrayList<>();
        //存储销售总额
        List<Object> totalAmount = new ArrayList<>();
        for (Map<String, Object> map : list) {

            groupByType.add(map.get("groupByType"));
            totalAmount.add(map.get("totalAmount"));

        }

        model.addAttribute("groupByType", JSON.toJSONString(groupByType));
        model.addAttribute("totalAmount", JSON.toJSONString(totalAmount));
        model.addAttribute("groupBy", SaleChartsQueryObject.groupByTypes.get(qo.getGroupBy()));

        return "charts/sale_bar";
    }
    @RequestMapping("sale_pie")
    @RequirePermission("饼状图")
    public String saleChartsByPie(Model model, @ModelAttribute("qo")SaleChartsQueryObject qo){

        List<Map<String, Object>> list  = service.selectSaleCharts(qo);
        //存储分组类型的所有数据
        List<Object> groupByType = new ArrayList<>();
        //存放 value 和name
        List<Map<String,Object>> mapList = new ArrayList<>();

        //金额的最大值
        BigDecimal maxSaleAmount = BigDecimal.ZERO;
        for (Map<String, Object> map : list) {
            groupByType.add(map.get("groupByType"));

            Map<String,Object> mapData = new HashMap<>();
            mapData.put("name",map.get("groupByType"));
            mapData.put("value",map.get("totalAmount"));

            mapList.add(mapData);
           BigDecimal totalAmount = (BigDecimal) map.get("totalAmount");
            if (maxSaleAmount.compareTo(totalAmount) <= 0){
                maxSaleAmount = totalAmount;
            }

        }
        model.addAttribute("maxSaleAmount",maxSaleAmount);
        model.addAttribute("mapList",JSON.toJSONString(mapList));
        model.addAttribute("groupByType", JSON.toJSONString(groupByType));
        model.addAttribute("groupBy", SaleChartsQueryObject.groupByTypes.get(qo.getGroupBy()));

        return "charts/sale_pie";
    }


}
