package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Client;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IClientService;
import cn.wolfcode.wms.utils.JsonResult;
import cn.wolfcode.wms.utils.RequirePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("client")
public class ClientController {

	@Autowired
	private IClientService service;

	@RequestMapping("list")
	@RequirePermission("客户列表")
	public String list(Model model ,@ModelAttribute("qo")QueryObject qo) {

		model.addAttribute("result", service.query(qo));
		return "client/list";
	}

	@RequestMapping("delete")
	@RequirePermission("客户删除")
	@ResponseBody
	public JsonResult delete(Long id) {
		JsonResult result = new JsonResult();
		if (id != null) {

			service.deleteByPrimaryKey(id);
		}
		return result;
	}

	@RequestMapping("input")
	@RequirePermission("客户编辑")
	public String input(Model model, Long id) {

		if (id != null) {
			model.addAttribute("client",service.selectByPrimaryKey(id));
		}
		return "client/input";
	}

	@RequestMapping("saveOrUpdate")
	@RequirePermission("客户保存或更新")
	@ResponseBody
	public JsonResult saveOrUpdate(Client client) {

		JsonResult result = new JsonResult();
		if (client.getId()!=null) {
			
			service.updateByPrimaryKey(client);
		}else{
			service.insert(client);
		}
		return result;
	}

}
