package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.SystemMenu;
import cn.wolfcode.wms.query.SystemMenuQueryObject;
import cn.wolfcode.wms.service.ISystemMenuService;
import cn.wolfcode.wms.utils.JsonResult;
import cn.wolfcode.wms.utils.RequirePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("systemMenu")
public class SystemMenuController {

    @Autowired
    private ISystemMenuService service;
    @RequestMapping("list")
    @RequirePermission("菜单列表")
    public String list(Model model, @ModelAttribute("qo") SystemMenuQueryObject qo){
        //根据父级菜单的id查询当前子菜单的信息
        SystemMenu parent = service.selectByPrimaryKey(qo.getParentId());
        List<Map<String,Object>> parents = new ArrayList<>();
        while(parent!=null){

            Map<String,Object> menu = new HashMap<>();
            //parent的id使用来查询当前父级菜单id下的所有子级菜单信息,
            menu.put("id",parent.getId());
            //parent的name使用来显示在页面上
            menu.put("name",parent.getName());
            parents.add(menu);
            //获取当前菜单的父级菜单(mybatis延迟 加载的特性)
            parent = parent.getParent();
        }

        Collections.reverse(parents);
        model.addAttribute("parents",parents);


        model.addAttribute("result", service.query(qo));
        return "systemMenu/list";
    }

    @RequestMapping("delete")
    @ResponseBody
    @RequirePermission("菜单删除")
    public JsonResult delete(Long id){
        JsonResult result = new JsonResult();
        try{
            if(id != null){
                service.deleteByPrimaryKey(id);
            }

        }catch (Exception e){
            e.printStackTrace();
            result.mark("删除失败!");
        }

        return result;
    }

    @RequestMapping("input")
    @RequirePermission("菜单编辑")
    public String input(Long id,Model model,Long parentId){
        //将父级id传递过来查询,若为空为根目录,若不为空,查询出父级目录的名称
        System.out.println(parentId);
        if(parentId==null){
            model.addAttribute("parentId",null);
            model.addAttribute("parentName","根目录");

        }else{
            SystemMenu parent = service.selectByPrimaryKey(parentId);
            model.addAttribute("parentName",parent.getName());
            model.addAttribute("parentId",parent.getId());

        }


        if(id!=null){
            SystemMenu systemMenu = service.selectByPrimaryKey(id);
            model.addAttribute("systemMenu",systemMenu);
        }

        return "systemMenu/input";
    }
    @RequestMapping("saveOrUpdate")
    @RequirePermission("菜单保存或更新")
    @ResponseBody
    public JsonResult saveOrUpdate(SystemMenu systemMenu){
        JsonResult result = new JsonResult();

        try{
            if(systemMenu.getId()!=null){
                service.updateByPrimaryKey(systemMenu);

            }else {
                service.insert(systemMenu);
            }
        }catch (Exception e){
            e.printStackTrace();
            result.mark("保存或更新失败");
        }
        return result;
    }
    @RequestMapping("selectMenuByParentSn")
    @ResponseBody
    public List<Map<String ,Object>> selectMenuByParentSn(String parentSn){
        List<Map<String ,Object>> childrenMenu =  service.selectMenuByParentSn(parentSn);
        return childrenMenu;
    }
}
