package cn.wolfcode.wms.web.controller;

import cn.wolfcode.wms.domain.Department;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IDepartmentService;
import cn.wolfcode.wms.utils.RequirePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("department")
public class DepartmentController {

	@Autowired
	private IDepartmentService service;

	@RequestMapping("list")
	@RequirePermission("部门列表")
	public String list(Model model ,@ModelAttribute("qo")QueryObject qo) {

		model.addAttribute("result", service.query(qo));
		return "department/list";
	}

	@RequestMapping("delete")
	@RequirePermission("部门删除")
	public String delete(Long id) {
		if (id != null) {

			service.deleteByPrimaryKey(id);
		}
		return "redirect:/department/list.do";
	}

	@RequestMapping("input")
	@RequirePermission("部门编辑")
	public String input(Model model, Long id) {

		if (id != null) {
			model.addAttribute("dept",service.selectByPrimaryKey(id));
		}
		return "department/input";
	}

	@RequestMapping("saveOrUpdate")
	@RequirePermission("部门保存或更新")
	public String saveOrUpdate(Department department) {
		
		if (department.getId()!=null) {
			
			service.updateByPrimaryKey(department);
		}else{
			service.insert(department);
		}
		return "redirect:/department/list.do";
	}

}
