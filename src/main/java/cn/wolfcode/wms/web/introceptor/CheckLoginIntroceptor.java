package cn.wolfcode.wms.web.introceptor;

import cn.wolfcode.wms.domain.Employee;
import cn.wolfcode.wms.utils.UserContext;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckLoginIntroceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        Employee employee = UserContext.getEmployee();
        if (employee == null){

            response.sendRedirect("/login.jsp");
            return  false;
        }
        return  true;
    }
}
