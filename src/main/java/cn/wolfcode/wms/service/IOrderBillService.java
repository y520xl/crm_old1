package cn.wolfcode.wms.service;

import cn.wolfcode.wms.domain.OrderBill;
import cn.wolfcode.wms.query.OrderBillQueryObject;
import cn.wolfcode.wms.utils.PageResult;

import java.util.List;

public interface IOrderBillService {
    void deleteByPrimaryKey(Long id);

    void insert(OrderBill entity);

    OrderBill selectByPrimaryKey(Long id);

    List<OrderBill> selectAll();

    void updateByPrimaryKey(OrderBill entity);

    PageResult query(OrderBillQueryObject qo);

    void audit(Long id);
}
