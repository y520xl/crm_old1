package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.ProductStock;
import cn.wolfcode.wms.domain.StockIncomeBill;
import cn.wolfcode.wms.domain.StockIncomeBillItem;
import cn.wolfcode.wms.mapper.ProductStockMapper;
import cn.wolfcode.wms.mapper.StockIncomeBillItemMapper;
import cn.wolfcode.wms.mapper.StockIncomeBillMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IStockIncomeBillService;
import cn.wolfcode.wms.utils.PageResult;
import cn.wolfcode.wms.utils.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class StockIncomeBillServiceImpl implements IStockIncomeBillService {

    @Autowired
    private StockIncomeBillMapper mapper;
    @Autowired
    private StockIncomeBillItemMapper stockIncomeBillItemMapper;

    @Autowired
    private ProductStockMapper productStockMapper;

    @Override
    public void deleteByPrimaryKey(Long id) {
        stockIncomeBillItemMapper.deleteByPrimaryKey(id);

        mapper.deleteByPrimaryKey(id);

    }

    @Override
    public void insert(StockIncomeBill entity) {

        //先保存入库单据
        entity.setStatus(StockIncomeBill.STATUS_NOMAL);
        entity.setInputTime(new Date());
        entity.setInputUser(UserContext.getEmployee());
        BigDecimal totalNumber = BigDecimal.ZERO;

        BigDecimal totalAmount = BigDecimal.ZERO;

        List<StockIncomeBillItem> items = entity.getItems();
        for (StockIncomeBillItem item : items) {

            //计算总数量
            totalNumber = totalNumber.add(item.getNumber());
            //计算总额
            totalAmount = totalAmount.add(item.getNumber().multiply(item.getCostPrice()));
        }

        entity.setTotalNumber(totalNumber);
        entity.setTotalAmount(totalAmount);
        mapper.insert(entity);

        //保存入库单明细
        for (StockIncomeBillItem item : items) {
            //设置入库单的id
            item.setBillId(entity.getId());
            item.setAmount(item.getNumber().multiply(item.getCostPrice()));
            stockIncomeBillItemMapper.insert(item);
        }


    }

    @Override
    public StockIncomeBill selectByPrimaryKey(Long id) {
        System.out.println(mapper);
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<StockIncomeBill> selectAll() {
        return mapper.selectAll();
    }

    @Override
    public void updateByPrimaryKey(StockIncomeBill entity) {

        StockIncomeBill old = mapper.selectByPrimaryKey(entity.getId());
        //当其状态为0时此时才可以更新数据
        if (old.getStatus() == StockIncomeBill.STATUS_NOMAL) {
            //先保存入库单据
            entity.setStatus(StockIncomeBill.STATUS_NOMAL);
            entity.setInputTime(new Date());
            entity.setInputUser(UserContext.getEmployee());
            BigDecimal totalNumber = BigDecimal.ZERO;

            BigDecimal totalAmount = BigDecimal.ZERO;

            List<StockIncomeBillItem> items = entity.getItems();
            for (StockIncomeBillItem item : items) {
                //保存明细
                item.setBillId(entity.getId());
                item.setAmount(item.getNumber().multiply(item.getCostPrice()));
                stockIncomeBillItemMapper.insert(item);


                //计算总数量
                totalNumber = totalNumber.add(item.getNumber());
                //计算总额
                totalAmount = totalAmount.add(item.getNumber().multiply(item.getCostPrice()));

            }

            entity.setTotalNumber(totalNumber);
            entity.setTotalAmount(totalAmount);
            mapper.updateByPrimaryKey(entity);

        }
    }

    @Override
    public PageResult query(QueryObject qo) {
        System.out.println("StockIncomeBillServiceImpl.query");
        Long totalCount = mapper.queryForCount(qo);
        if (totalCount == 0) {
            return new PageResult(qo.getPageSize());
        }
        List<StockIncomeBill> listData = mapper.queryForList(qo);
        return new PageResult(listData, totalCount, qo.getCurrentPage(), qo.getPageSize());
    }

    /**
     * 入库单审核之后商品得入库
     * @param id
     */
    @Override
    public void audit(Long id) {
        //从数据库查询出该id值单据的审核状态,如果已经审核,则没必要审核  审核就入库
        StockIncomeBill bill = mapper.selectByPrimaryKey(id);
        if (bill.getStatus() == StockIncomeBill.STATUS_NOMAL) {
            //此状态为未审核
            //审核人
            bill.setAuditor(UserContext.getEmployee());
            //审核时间
            bill.setAuditTime(new Date());
            //审核状态
            bill.setStatus(StockIncomeBill.STATUS_AUDIT);
            mapper.audit(bill);

            //修改库存信息  入库单审核之后直接入库
            List<StockIncomeBillItem> items = bill.getItems();
            for (StockIncomeBillItem item : items) {
                Long productId = item.getProduct().getId();
                Long depotId = bill.getDepot().getId();
                ProductStock ps = productStockMapper.selectByProductIdAndDepotId(productId,depotId);
                //如果没有的话,则直接将数据保存到入库中
                if (ps == null){

                    ps = new ProductStock();
                    ps.setStoreNumber(item.getNumber());
                    ps.setPrice(item.getCostPrice());
                    ps.setAmount(item.getNumber().multiply(item.getCostPrice()));
                    ps.setProduct(item.getProduct());
                    ps.setDepot(bill.getDepot());

                    productStockMapper.insert(ps);
                }else {

                    //否则,则修改数据即可
                    ps.setStoreNumber(ps.getStoreNumber().add(item.getNumber()));
                    ps.setAmount(ps.getAmount().add(item.getNumber().multiply(item.getCostPrice())));
                    BigDecimal price = ps.getAmount().divide(ps.getStoreNumber());
                    ps.setPrice(price);
                    productStockMapper.updateByPrimaryKey(ps);
                }

            }
        }




    }


}
