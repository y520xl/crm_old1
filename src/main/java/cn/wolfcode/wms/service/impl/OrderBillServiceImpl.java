package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.Employee;
import cn.wolfcode.wms.domain.OrderBill;
import cn.wolfcode.wms.domain.OrderBillItem;
import cn.wolfcode.wms.mapper.OrderBillItemMapper;
import cn.wolfcode.wms.mapper.OrderBillMapper;
import cn.wolfcode.wms.query.OrderBillQueryObject;
import cn.wolfcode.wms.service.IOrderBillService;
import cn.wolfcode.wms.utils.PageResult;
import cn.wolfcode.wms.utils.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class OrderBillServiceImpl implements IOrderBillService {

    @Autowired
    private OrderBillMapper mapper;
    @Autowired
    private OrderBillItemMapper orderBillItemMapper;


    @Override
    public void deleteByPrimaryKey(Long id) {
        orderBillItemMapper.deleteByPrimaryKey(id);

        mapper.deleteByPrimaryKey(id);

    }

    @Override
    public void insert(OrderBill entity) {
        //封装订单信息:
        //状态值
        entity.setStatus(OrderBill.STATUS_NOMAL);
        //录入时间
        entity.setInputTime(new Date());
        //录入人
        Employee employee = UserContext.getEmployee();
        entity.setInputUser(employee);
        //计算总数量和总额
        List<OrderBillItem> items = entity.getItems();
        //总数量
        BigDecimal totalNumber = BigDecimal.ZERO;
        //总额
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (OrderBillItem item : items) {
            totalNumber = totalNumber.add(item.getNumber());
            totalAmount = totalAmount.add(item.getNumber().multiply(item.getCostPrice()));

        }
        entity.setTotalNumber(totalNumber);
        entity.setTotalAmount(totalAmount);
        mapper.insert(entity);
        //设置明细操作
        for (OrderBillItem item : items) {

            item.setBillId(entity.getId());
            item.setAmount(item.getCostPrice().multiply(item.getNumber()));
            orderBillItemMapper.insert(item);
        }

    }

    @Override
    public OrderBill selectByPrimaryKey(Long id) {
        System.out.println(mapper);
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<OrderBill> selectAll() {
        return mapper.selectAll();
    }

    @Override
    public void updateByPrimaryKey(OrderBill entity) {
        //删除原来的明细信息
        orderBillItemMapper.deleteByBillId(entity.getId());

        //从数据库查询出该id值单据的审核状态,如果已经审核,则没必要更新数据信息
        OrderBill old = mapper.selectByPrimaryKey(entity.getId());
        if (old.getStatus() == OrderBill.STATUS_NOMAL) {


            //重新封装订单信息
            //计算总数量和总额
            List<OrderBillItem> items = entity.getItems();
            //总数量
            BigDecimal totalNumber = BigDecimal.ZERO;
            //总额
            BigDecimal totalAmount = BigDecimal.ZERO;
            for (OrderBillItem item : items) {
                totalNumber = totalNumber.add(item.getNumber());
                totalAmount = totalAmount.add(item.getNumber().multiply(item.getCostPrice()));

                //明细的信息
                item.setBillId(entity.getId());
                item.setAmount(item.getCostPrice().multiply(item.getNumber()));
                orderBillItemMapper.insert(item);
            }

            entity.setTotalNumber(totalNumber);
            entity.setTotalAmount(totalAmount);
            mapper.updateByPrimaryKey(entity);
        }
    }

    @Override
    public PageResult query(OrderBillQueryObject qo) {

        Long totalCount = mapper.queryForCount(qo);
        if (totalCount == 0) {
            return new PageResult(qo.getPageSize());
        }
        List<OrderBill> listData = mapper.queryForList(qo);
        return new PageResult(listData, totalCount, qo.getCurrentPage(), qo.getPageSize());
    }


    @Override
    public void audit(Long id) {
        //从数据库查询出该id值单据的审核状态,如果已经审核,则没必要审核
        OrderBill old = mapper.selectByPrimaryKey(id);
        if (old.getStatus() == OrderBill.STATUS_NOMAL) {
            //次状态为未审核
            //审核人
            old.setAuditor(UserContext.getEmployee());
            //审核时间
            old.setAuditTime(new Date());
            //审核状态
            old.setStatus(OrderBill.STATUS_AUDIT);
            mapper.audit(old);

        }

    }


}
