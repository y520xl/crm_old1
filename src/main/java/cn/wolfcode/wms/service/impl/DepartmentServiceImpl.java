package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.Department;
import cn.wolfcode.wms.mapper.DepartmentMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IDepartmentService;
import cn.wolfcode.wms.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements IDepartmentService {

    @Autowired
    private DepartmentMapper mapper;

    @Override
    public void deleteByPrimaryKey(Long id) {

        mapper.deleteByPrimaryKey(id);
    }

    @Override
    public void insert(Department entity) {
        mapper.insert(entity);
    }

    @Override
    public Department selectByPrimaryKey(Long id) {
        System.out.println(mapper);
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Department> selectAll() {
        return mapper.selectAll();
    }

    @Override
    public void updateByPrimaryKey(Department entity) {
        mapper.updateByPrimaryKey(entity);
    }

    @Override
    public PageResult query(QueryObject qo) {

        Long totalCount = mapper.queryForCount(qo);
        if (totalCount == 0){
            return new PageResult(qo.getPageSize());
        }
        List<Department> listData = mapper.queryForList(qo);
        return new PageResult(listData,totalCount,qo.getCurrentPage(),qo.getPageSize());
    }


}
