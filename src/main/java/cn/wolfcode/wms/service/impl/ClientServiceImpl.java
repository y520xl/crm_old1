package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.Client;
import cn.wolfcode.wms.mapper.ClientMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.IClientService;
import cn.wolfcode.wms.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements IClientService {

    @Autowired
    private ClientMapper mapper;

    @Override
    public void deleteByPrimaryKey(Long id) {

        mapper.deleteByPrimaryKey(id);
    }

    @Override
    public void insert(Client entity) {
        mapper.insert(entity);
    }

    @Override
    public Client selectByPrimaryKey(Long id) {
        System.out.println(mapper);
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Client> selectAll() {
        return mapper.selectAll();
    }

    @Override
    public void updateByPrimaryKey(Client entity) {
        mapper.updateByPrimaryKey(entity);
    }

    @Override
    public PageResult query(QueryObject qo) {

        Long totalCount = mapper.queryForCount(qo);
        if (totalCount == 0){
            return new PageResult(qo.getPageSize());
        }
        List<Client> listData = mapper.queryForList(qo);
        return new PageResult(listData,totalCount,qo.getCurrentPage(),qo.getPageSize());
    }


}
