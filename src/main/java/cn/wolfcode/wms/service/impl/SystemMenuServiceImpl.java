package cn.wolfcode.wms.service.impl;

import cn.wolfcode.wms.domain.Employee;
import cn.wolfcode.wms.domain.SystemMenu;
import cn.wolfcode.wms.mapper.SystemMenuMapper;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.service.ISystemMenuService;
import cn.wolfcode.wms.utils.PageResult;
import cn.wolfcode.wms.utils.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SystemMenuServiceImpl implements ISystemMenuService {

    @Autowired
    private SystemMenuMapper mapper;

    @Override
    public void deleteByPrimaryKey(Long id) {

        mapper.deleteByPrimaryKey(id);
    }

    @Override
    public void insert(SystemMenu entity) {
        mapper.insert(entity);
    }

    @Override
    public SystemMenu selectByPrimaryKey(Long id) {
        System.out.println(mapper);
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SystemMenu> selectAll() {
        return mapper.selectAll();
    }

    @Override
    public void updateByPrimaryKey(SystemMenu entity) {
        mapper.updateByPrimaryKey(entity);
    }

    @Override
    public PageResult query(QueryObject qo) {

        Long totalCount = mapper.queryForCount(qo);
        if (totalCount == 0){
            return new PageResult(qo.getPageSize());
        }
        List<SystemMenu> listData = mapper.queryForList(qo);
        return new PageResult(listData,totalCount,qo.getCurrentPage(),qo.getPageSize());
    }

    @Override
    public List<Map<String, Object>> selectMenuByParentSn(String parentSn) {

        Employee employee = UserContext.getEmployee();
        if (employee.isAdmin()){
            return  mapper.selectMenuByParentSn(parentSn);
        }

        return mapper.selectMenuByParentSnAndEmpId(parentSn,employee.getId());
    }


}
