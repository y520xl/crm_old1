package cn.wolfcode.wms.service;

import cn.wolfcode.wms.query.ProductStockQueryObject;
import cn.wolfcode.wms.utils.PageResult;

public interface IProductStockService {

    PageResult query(ProductStockQueryObject qo);

}
