package cn.wolfcode.wms.service;

import cn.wolfcode.wms.domain.SystemMenu;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.utils.PageResult;

import java.util.List;
import java.util.Map;

public interface ISystemMenuService {
    void deleteByPrimaryKey(Long id);

    void insert(SystemMenu entity);

    SystemMenu selectByPrimaryKey(Long id);

    List<SystemMenu> selectAll();

    void updateByPrimaryKey(SystemMenu entity);

    PageResult query(QueryObject qo);

    /**
     * 根据父级菜单的sn来查询子菜单
     * @param parentSn 父菜单的sn编号
     * @return 返回所有额子菜单list集合
     */

    List<Map<String,Object>> selectMenuByParentSn(String parentSn);
}
