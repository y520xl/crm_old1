package cn.wolfcode.wms.service;

import cn.wolfcode.wms.domain.Employee;
import cn.wolfcode.wms.query.QueryObject;
import cn.wolfcode.wms.utils.PageResult;

import java.util.List;

public interface IEmployeeService {
    void deleteByPrimaryKey(Long id);

    void insert(Employee entity,Long[] roleIds);

    Employee selectByPrimaryKey(Long id);

    List<Employee> selectAll();

    void updateByPrimaryKey(Employee entity,Long[] roleIds);

    PageResult query(QueryObject qo);

    /**
     * 批量删除
     * @param ids
     */
    void batchDelete(Long[] ids);

    /**
     * 登录判断
     * @param name
     * @param password
     */
    void login(String name, String password);
}
