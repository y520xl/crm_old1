package cn.wolfcode.wms.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Supplier extends BaseDomain {
    private String name;
    private String phone;
    private String address;
}