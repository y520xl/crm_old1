package cn.wolfcode.wms.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@ToString
public class StockIncomeBill extends BaseBillDomain {
    private Depot depot;//仓库
    //订单明细:一对多
    private List<StockIncomeBillItem> items = new ArrayList<>();
}
