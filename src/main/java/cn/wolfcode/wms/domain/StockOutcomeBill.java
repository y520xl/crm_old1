package cn.wolfcode.wms.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@ToString
public class StockOutcomeBill extends BaseBillDomain {
    private Depot depot;//仓库
    private Client client;//客户
    //订单明细:一对多
    private List<StockOutcomeBillItem> items = new ArrayList<>();
}
