package cn.wolfcode.wms.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

//订单对象
@Setter
@Getter
@ToString
public class OrderBill extends BaseBillDomain {
    private Supplier supplier;//供应商
    //订单明细:一对多
    private List<OrderBillItem> items = new ArrayList<>();
}
