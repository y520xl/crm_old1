package cn.wolfcode.wms.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

//员工对象
@Setter
@Getter
public class Employee extends BaseDomain {
    private String name;
    private String password;
    private String email;
    private Integer age;
    private boolean admin = false;
    private Department dept;
    List<Employee> roles = new ArrayList<>();
}