package cn.wolfcode.wms.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@ToString
public class BaseBillDomain {
    public static final int STATUS_NOMAL = 0;
    public static final int STATUS_AUDIT = 1;

    private Long id;//单据的id
    private String sn;//单据的编号
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date vdate; //单据的业务时间
    private int status = STATUS_NOMAL; //单据的默认状态为未审核状态

    private BigDecimal totalAmount;//总金额
    private BigDecimal totalNumber;//总数量

    private Date auditTime;//审核时间
    private Date inputTime;//录入时间

    private Employee inputUser;//录入人
    private Employee auditor;//审核人
    private Date birthDate; //出生日期
    private String brthDate; //出生日期
}
