package cn.wolfcode.wms.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Setter@Getter
public class OrderBillQueryObject extends QueryObject{

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date beginDate;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date endDate;

    private Long  supplierId = -1L;
    private int status = -1;
/*
    public Date getEndDate(){
        return DateUtil.getEndDate(endDate);
    }*/
}
