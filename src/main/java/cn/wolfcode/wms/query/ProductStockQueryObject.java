package cn.wolfcode.wms.query;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class ProductStockQueryObject extends QueryObject {
    private String keywords;
    private Long depotId = -1L;
    private Long brandId = -1L;
    private Long limitNumber;

    public String getKeywords(){

        return empty2null(keywords);
    }

}
