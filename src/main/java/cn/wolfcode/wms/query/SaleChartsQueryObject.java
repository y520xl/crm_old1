package cn.wolfcode.wms.query;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Setter@Getter@ToString
public class SaleChartsQueryObject {


    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date beginDate;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date endDate;

    private Long  clientId = -1L;

    private Long brandId = -1L;

    private String groupBy = "iu.name";
    public static Map<String,String> groupByTypes = new LinkedHashMap();

    static {
            groupByTypes.put("iu.name","订货人员");
            groupByTypes.put("p.name","商品名称");
            groupByTypes.put("c.name","客户");
            groupByTypes.put("p.brand_name","品牌");
            groupByTypes.put("DATE_FORMAT(bill.vdate,'%Y,%m,')","订货日期(月)");
            groupByTypes.put("DATE_FORMAT(bill.vdate,'%Y,%m,%d')","订货日期(日)");
    }
}
