package cn.wolfcode.wms.query;

import com.alibaba.druid.util.StringUtils;
import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class QueryObject {
    private  Long currentPage =1L;
    private Long pageSize = 5L;

    public Long  getStart(){
        return (currentPage - 1) * pageSize;

    }

    public String empty2null(String str){

        return StringUtils.isEmpty(str) ? null : str;
    }
}
