package cn.wolfcode.wms.query;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmployeeQueryObject extends QueryObject {
    private String keywords;
    private Long deptId;

    public String getKeywords() {
        return empty2null(keywords);
    }
}
