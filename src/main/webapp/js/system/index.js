//加载当前日期
function loadDate(){
	var time = new Date();
	var myYear = time.getFullYear();
	var myMonth = time.getMonth() + 1;
	var myDay = time.getDate();
	if (myMonth < 10) {
		myMonth = "0" + myMonth;
	}
	document.getElementById("day_day").innerHTML = myYear + "." + myMonth + "."	+ myDay;
}

/**
 * 隐藏或者显示侧边栏
 * 
 **/
function switchSysBar(flag){
	var side = $('#side');
    var left_menu_cnt = $('#left_menu_cnt');
	if( flag==true ){	// flag==true
		left_menu_cnt.show(500, 'linear');
		side.css({width:'280px'});
		$('#top_nav').css({width:'77%', left:'304px'});
    	$('#main').css({left:'280px'});
	}else{
        if ( left_menu_cnt.is(":visible") ) {
			left_menu_cnt.hide(10, 'linear');
			side.css({width:'60px'});
        	$('#top_nav').css({width:'100%', left:'60px', 'padding-left':'28px'});
        	$('#main').css({left:'60px'});
        	$("#show_hide_btn").find('img').attr('src', 'images/common/nav_show.png');
        } else {
			left_menu_cnt.show(500, 'linear');
			side.css({width:'280px'});
			$('#top_nav').css({width:'77%', left:'304px', 'padding-left':'0px'});
        	$('#main').css({left:'280px'});
        	$("#show_hide_btn").find('img').attr('src', 'images/common/nav_hide.png');
        }
	}
}

$(function(){
	loadDate();
	
	// 显示隐藏侧边栏
	$("#show_hide_btn").click(function() {
        switchSysBar();
    });
});


var setting = {
    data: {
        simpleData: {
            enable: true
        }
    },
    callback:{
        onClick:function (event, treeId, treeNode) {
        	$("#rightMain").prop("src",treeNode.controller);

        }
    },
    async: {
        enable: true,
        url:"/systemMenu/selectMenuByParentSn.do",
        autoParam:["sn=parentSn"]
    }

};

var zNodes = {
    business: [
		{id: 1, pId: 0, name: "业务模块", open: false ,isParent:true,sn:"business"}

    ], system: [
        {id: 2, pId: 0, name: "系统模块", open: false,isParent:true,sn:"system"}
    ], charts: [
        {id: 3, pId: 0, name: "报表模块", open: false,isParent:true,sn:"charts"}
    ]

}
$(function () {

    $.fn.zTree.init($("#dleft_tab1"), setting, zNodes.business);
})


$(function () {

    $("#TabPage2 li").click(function () {
		//1.先将每一个li中的class属性的值去掉
        $("#TabPage2 li").each(function (index,item) {

        	$(item).removeClass("selected");
        	//将所用的元素修改为最初状态
			$(item).children("img").prop("src","/images/common/"+(index + 1)+".jpg");
        })
		//2.为当前的li 元素添加selected
		$(this).addClass("selected");

        //3.切换选中选项的图片
        $(this).children("img").prop("src",("/images/common/"+($(this).index() +1) +"_hover.jpg"))
		//4.切换模块的图片
		$("#nav_module").children("img").prop("src","/images/common/module_"+($(this).index() + 1)+".png")
		//5.为当前位置设置信息
		$("#here_area").html("当前位置：系统&nbsp;>&nbsp;"+$(this).prop("title"));

        //6.切换菜单
		var url = $(this).data("rootmenu");
        $.fn.zTree.init($("#dleft_tab1"), setting, zNodes[url]);

    })

})

