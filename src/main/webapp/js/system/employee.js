
//页面加载事件
$(function () {

    //全部右移
    $("#selectAll").click(function () {

        $(".allRoles option").appendTo($(".selectedRoles"));
    })
    //全部左移
    $("#deselectAll").click(function () {

        $(".selectedRoles option").appendTo($(".allRoles"));
    })

    //选中的右移
    $("#select").click(function () {

        $(".allRoles option:selected").appendTo($(".selectedRoles"));
    })

    //选中的左移
    $("#deselect").click(function () {

        $(".selectedRoles option:selected").appendTo($(".allRoles"));
    })

    //右侧已经选中的在左侧被删除
    //x先获取右侧选择的
    var ids = [];
    $(".selectedRoles option").each(function (index,item) {

        var id = $(item).val();
        ids.push(id);
    })
    //获取右侧所有的和左侧匹对,右侧有的就删除
    $(".allRoles option").each(function (index,item) {

        if($.inArray(item.value,ids) > -1){
            $(item).remove();
        }
    })
    //将其右侧所有的选中的数据提交

    $(".editForm").submit(function () {

        $(".selectedRoles option").each(function (index,item) {

            $(item).prop("checked",true);
        })
    })
})