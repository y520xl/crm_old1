$(function () {
    //选中的右移
    $("#mselect").click(function () {
        $(".allSystemMenu option:selected").appendTo($(".selectedSystemMenu"));

    })
    //选中的走移
    $("#mdeselect").click(function () {
        $(".selectedSystemMenu option:selected").appendTo($(".allSystemMenu"));

    })
    //全部右移
    $("#mselectAll").click(function () {
        $(".allSystemMenu option").appendTo($(".selectedSystemMenu"));
    })
    //全部左移
    $("#mdeselectAll").click(function () {

        $(".selectedSystemMenu option").appendTo($(".allSystemMenu"));
    })

})

//提交菜单
$(function () {
    //删除已经存在的左边值
    var mids =[];
    $(".selectedSystemMenu option").each(function (index,item) {

        var id = $(item).val();
        mids.push(id);
    })
    $(".allSystemMenu option").each(function (index,item) {
       if($.inArray($(item).val(),mids)>-1){
           $(item).remove();
       }
    })

    //将右侧已经选中的权限通过表单提交
    $("#editForm").submit(function () {

        $(".selectedSystemMenu option").each(function (index,item) {

            $(item).prop("selected",true);
        })
    })
})


$(function () {
    //选中的右移
    $("#select").click(function () {
        $(".allPermission option:selected").appendTo($(".selectedPermission"));

    })
    //选中的走移
    $("#deselect").click(function () {
        $(".selectedPermission option:selected").appendTo($(".allPermission"));

    })
    //全部右移
    $("#selectAll").click(function () {
        $(".allPermission option").appendTo($(".selectedPermission"));
    })
    //全部左移
    $("#deselectAll").click(function () {

        $(".selectedPermission option").appendTo($(".allPermission"));
    })

})
//提交权限
$(function () {
    //删除已经存在的左边值
    var ids =[];
    $(".selectedPermission option").each(function (index,item) {

        var id = $(item).val();
        ids.push(id);
    })

    $(".allPermission option").each(function (index,item) {
       if($.inArray($(item).val(),ids)>-1){
           $(item).remove();
       }
    })

    //将右侧已经选中的权限通过表单提交
    $("#editForm").submit(function () {

        $(".selectedPermission option").each(function (index,item) {

            $(item).prop("selected",true);
        })
    })
})