/** table鼠标悬停换色* */
jQuery.ajaxSettings.traditional = true;
$(function () {
    // 如果鼠标移到行上时，执行函数
    $(".table tr").mouseover(function () {
        $(this).css({background: "#CDDAEB"});
        $(this).children('td').each(function (index, ele) {
            $(ele).css({color: "#1D1E21"});
        });
    }).mouseout(function () {
        $(this).css({background: "#FFF"});
        $(this).children('td').each(function (index, ele) {
            $(ele).css({color: "#909090"});
        });
    });
});


$(function () {
    //查询:
    $(".btn_reseacher").click(function () {

        $("#searchForm").submit();
    })

    //新增操作
    $(".btn_input").click(function () {
        window.location.href = $(this).data("url") ;
    })
    //提交表单
    $(".btn_page").click(function () {
        //获取每一个按钮上当前页的值
        var currentPage = $(this).data("page") || 1;
        //将值赋给currentPage页面上
        $(":input[name='currentPage']").val(currentPage);
        //提交表单
        $("#searchForm").submit();
    })
    //值改变事件
    $(":input[name='pageSize'] option").click(function () {
        $(":input[name='currentPage']").val(1);
        $("#searchForm").submit();

    })

    //为下拉列表设置默认值
    $(":input[name='pageSize'] option").each(function (index, item) {
    var pageSize = $(":input[name='pageSize']").data("pagesize");

        if (item.innerHTML == pageSize) {
            $(item).prop("selected", true);
        }
    })

})

//使用ajax删除指定id的数据
$(function () {

    $(".btn_delete").click(function () {

        var url = $(this).data("url");
        $.dialog({
            title:"温馨提示",
            content:"你确定要删除",
            drag:false,
            lock:true,
            icon:"face-sad",
            ok:function () {
                //发送ajax请求
                $.get(url,function (data) {
                    if(data.success){
                        $.dialog({
                            title: "温馨提示",
                            content: "删除成功!",
                            icon:"face-sad",
                            ok:function () {
                                window.location.reload();
                            }
                        })
                    }

                })
            },
            cancel:true
        })

    })
})
//订单的审核
$(function () {

    $(".btn_audit").click(function () {
        var url = $(this).data("url");
        $.dialog({
            title:"温馨提示",
            content:"你确定要审核吗",
            drag:false,
            lock:true,
            icon:"face-sad",
            ok:function () {
                //发送ajax请求
                $.get(url,function (data) {
                    if(data.success){
                        $.dialog({
                            title: "温馨提示",
                            content: "审核成功!",
                            icon:"face-sad",
                            ok:function () {
                                window.location.reload();
                            }
                        })
                    }

                })
            },
            cancel:true
        })

    })
})

//批量删除
$(function () {
    //每次将被选中的复选框给取消掉
    $("input:checked").prop("checked",false);

    $(".btn_batchDelete").click(function () {
        //如果用户没有选择要删除的数据,则给一个提示
        if ($(".acb:checked").size()==0){
            $.dialog({
                title: "温馨提示",
                content: "请选择要删除的数据",
                ok:true,
                drag: false,
                lock: true,
                icon:"face-smile"
            })
        }
        //选择要删除的数据

        var url = $(this).data("url");
        //获取要删除数据的id
        var ids = [];
        $(".acb:checked").each(function (index,item) {
            ids.push( $(item).data("id"));
        });

        $.dialog({
            title:"温馨提示",
            content:"你确定要删除选中的数据嘛?",
            drag:false,
            lock:true,
            icon:"face-sad",
            ok:function () {
                //发送ajax请求
                $.get(url,{ids:ids},function (data) {
                    if(data.success){
                        $.dialog({
                            title: "温馨提示",
                            content: "删除成功!",
                            icon:"face-sad",
                            ok:function () {
                                window.location.reload();
                            }
                        })
                    }

                })
            },
            cancel:true
        })
    })
    //全选或不全
    $("#all").change(function () {
        $(".acb").prop("checked",this.checked);
    })




})
//使用ajaxForm提交表单
$(function () {

    $("#editForm").ajaxForm(function (data) {
        var url = $("#editForm").data("url");
        if (data.success) {
            $.dialog({
                title: "温馨提示",
                content: "确定保存这些数据?",
                icon: "face-smile",
                lock: true,
                ok: function () {
                    window.location.href = url;
                }

            })
        }
    })
})


