<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/js/commonAll.js"></script>
    <title>叩丁狼教育PSS（演示版）-菜单管理</title>
    <style>
        .alt td{ background:black !important;}
    </style>

</head>
<body>
<form id="searchForm" action="/systemMenu/list.do" method="post">
    <input type="hidden" name="parentId" value="${qo.parentId}">

    <div id="container">
        <div class="ui_content">
            <div class="ui_text_indent">
                <div id="box_border">
                    <div id="box_top">搜索</div>
                    <div id="box_bottom">
                        <input type="button" value="新增" class="ui_input_btn01 btn_input"
                               data-url="/systemMenu/input.do?parentId=${qo.parentId}"/>
                    </div>
                </div>
            </div>
        </div>
       当前菜单:<a href="/systemMenu/list.do">根菜单</a>
        <c:forEach items="${parents}" var="parent">
        > <a href="/systemMenu/list.do?parentId=${parent.id}">${parent.name}</a>
        </c:forEach>

        <div class="ui_content">
            <div class="ui_tb">
                <table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <th width="30"><input type="checkbox" id="all"/></th>
                        <th>编号</th>
                        <th>菜单名称</th>
                        <th>菜单编码</th>
                        <th>父级菜单</th>
                        <th>URL</th>
                        <th></th>
                    </tr>
                    <tbody>
                    <c:forEach items="${result.listData}" var="ele">
                    <tr>
                        <td><input type="checkbox" name="IDCheck" class="acb" /></td>
                        <td>${ele.id}</td>
                        <td>${ele.name}</td>
                        <td>${ele.sn}</td>
                        <td>${ele.parent.name}</td>
                        <td>${ele.url}</td>
                        <td>
                            <a href="/systemMenu/input.do?id=${ele.id}&parentId=${qo.parentId}">编辑</a>
                            <a href="javascript:;" data-url="/systemMenu/delete.do?id=${ele.id}" class="btn_delete">删除</a>
                            <a href="/systemMenu/list.do?parentId=${ele.id}">下级菜单</a>
                        </td>
                    </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
           <jsp:include page="/WEB-INF/views/common/common_page.jsp"/>
        </div>
    </div>
</form>
</body>
</html>
