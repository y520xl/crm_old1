<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/js/jquery/plugins/ajax-form/jquery.form.min.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/My97DatePicker/WdatePicker.js"></script>

    <script type="text/javascript" src="/js/commonAll.js"></script>
    <title>叩丁狼教育PSS（演示版）-部门管理</title>
    <style>
        .alt td{ background:black !important;}
    </style>

</head>
<body>
<form id="searchForm" action="/orderBill/list.do" method="post">
    <div id="container">
        <div class="ui_content">
            <div class="ui_text_indent">
                <div id="box_border">
                    <div id="box_top">搜索</div>
                    <div id="box_center">
                        <fmt:formatDate value="${qo.beginDate}" pattern="yyyy-MM-dd" var="beginDate"/>
                        <fmt:formatDate value="${qo.endDate}" pattern="yyyy-MM-dd" var="endDate"/>

                    开始时间<input name="beginDate" onclick="WdatePicker();" value="" class="ui_input_txt02 Wdate beginDate" value="${beginDate}"/>~
                    结束时间<input name="endDate" onclick="WdatePicker();" value="" class="ui_input_txt02 Wdate endDate" value="${endDate}"/>
                    供应商
                    <select name="supplierId" class="ui_select01">
                        <option value="-1">所有供应商</option>
                        <c:forEach items="${suppliers}" var="supplier">

                            <option value="${supplier.id}" ${supplier.id == qo.supplierId ? "selected":""}>${supplier.name}</option>

                        </c:forEach>

                    </select>
                    状态
                    <select id="status" name="status" class="ui_select01">
                        <option value="-1" >所有状态</option>
                        <option value="0" ${qo.status == 0? "selected":""}>待审核</option>
                        <option value="1" ${qo.status == 1? "selected":""}>已审核</option>
                    </select>
                    <input type="button" value="查询" class="ui_input_btn01 btn_page" data-page="1"/>
                </div>

                <div id="box_bottom">
                        <input type="button" value="新增" class="ui_input_btn01 btn_input" data-url="/orderBill/input.do"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui_content">
            <div class="ui_tb">
                <table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <th width="30"><input type="checkbox" id="all"/></th>
                        <th>订单编码</th>
                        <th>业务时间</th>
                        <th>供应商</th>
                        <th>总金额</th>
                        <th>总数量</th>
                        <th>录入人</th>
                        <th>审核人</th>
                        <th>状态</th>
                        <th></th>
                    </tr>
                    <tbody>
                    <c:forEach items="${result.listData}" var="ele">
                    <tr>
                        <td><input type="checkbox" name="IDCheck" class="acb" /></td>
                        <td>${ele.sn}</td>
                        <td><fmt:formatDate value="${ele.vdate}"/></td>
                        <td>${ele.supplier.name}</td>
                        <td>${ele.totalAmount}</td>
                        <td>${ele.totalNumber}</td>
                        <td>${ele.inputUser.name}</td>
                        <td>${ele.auditor.name}</td>
                        <td>
                            <c:if test="${ele.status == 0}">
                                <span style="color: red">未审核</span>

                            </c:if>
                            <c:if test="${ele.status == 1}">
                                <span style="color: green">已审核</span>

                            </c:if>

                        </td>
                        <td>
                            <c:if test="${ele.status == 0}">

                            <a href="/orderBill/input.do?id=${ele.id}">编辑</a>
                            <a href="javascript:;" data-url="/orderBill/delete.do?id=${ele.id}" class="btn_delete">删除</a>
                            <a href="javascript:;" data-url="/orderBill/audit.do?id=${ele.id}" class="btn_audit">审核</a>

                            </c:if>
                            <c:if test="${ele.status == 1}">

                            <a href="/orderBill/viewBill.do?id=${ele.id}">查看</a>
                            </c:if>

                        </td>
                    </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
           <jsp:include page="/WEB-INF/views/common/common_page.jsp"/>
        </div>
    </div>
</form>
</body>
</html>
