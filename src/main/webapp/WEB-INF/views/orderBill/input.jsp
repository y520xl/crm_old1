<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>信息管理系统</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/js/jquery/plugins/ajax-form/jquery.form.min.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/plugins/iframeTools.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/My97DatePicker/WdatePicker.js"></script>

    <script type="text/javascript">
        <!--放大镜效果-->
        $(function () {
            $(".searchproduct").click(function () {
                var trObject = $(this).closest("tr");
                url = "/product/selectProductList.do";
                $.dialog.open(url, {
                        id: 'evaluate',
                        title: '选择商品列表',
                        width: 900,
                        height: 600,
                        left: '50%',
                        top: '50%',
                        background: '#000000',
                        opacity: 0.1,//透明度
                        lock: true,
                        resize: false,
                        close: function () {
                            var json = $.dialog.data("json");
                            //使用元素标签加上属性名的过滤方式

                            //此处有缓冲
                            if(json){

                                trObject.find("input[tag='pid']").val(json.id);
                                trObject.find("input[tag='costPrice']").val(json.costPrice);
                                trObject.find("input[tag='name']").val(json.name);
                                trObject.find("span[tag='brand']").text(json.brandName);
                                //清除缓冲
                                $.dialog.data("json","");
                            }
                        }
                    },
                    false);
            })

        })

        $(function () {

            //计算明细
            $("input[tag='costPrice'],input[tag='number']").blur(function () {

                var trObject = $(this).closest("tr");
                var costPrice = parseFloat(trObject.find("input[tag='costPrice']").val()) || 0;
                var number = parseFloat(trObject.find("input[tag='number']").val()) || 0;

                trObject.find("span[tag='amount']").text((costPrice * number).toFixed(2));

            })
            //多条明细
            $(".appendRow").click(function () {
                //克隆第一条数据
                var newTr = $("#edit_table_body tr:first").clone(true);
                //将第一行中的数据全部清零
                newTr.find("input[tag='pid']").val("");
                newTr.find("input[tag='name']").val("");
                newTr.find("input[tag='costPrice']").val("");
                newTr.find("span[tag='brand']").text("");
                newTr.find("input[tag='number']").val("");
                newTr.find("input[tag='remark']").val("");
                newTr.find("span[tag='amount']").text("");
                $("#edit_table_body").append(newTr);

            })

            //在提交表单之前修改表单元素的name属性
            $("#editForm").submit(function () {

                $("#edit_table_body tr").each(function (index, item) {

                    $(item).find("input[tag='pid']").prop("name", "items[" + index + "].product.id");
                    $(item).find("input[tag='costPrice']").prop("name", "items[" + index + "].costPrice");
                    $(item).find("input[tag='number']").prop("name", "items[" + index + "].number");
                    $(item).find("input[tag='remark']").prop("name", "items[" + index + "].remark");
                })

            })

            //删除名细
            $(".removeItem").click(function () {

                var currentTr =  $(this).closest("tr");
                if($("#edit_table_body tr").size() == 1){
                   //如果只有一条明细,此时点击删除明细时,那么此时就出现将这行tr中的数据全部清空
                   currentTr.find("input[tag='pid']").val("");
                   currentTr.find("input[tag='name']").val("");
                   currentTr.find("input[tag='costPrice']").val("");
                   currentTr.find("span[tag='amount']").text("");
                   currentTr.find("input[tag='remark']").val("");
                   currentTr.find("input[tag='number']").val("");
                   currentTr.find("span[tag='brand']").text("");
                }else{
                    //如果非一条数据,此时将该tr删除
                    currentTr.remove();
                }

            })

        })

    </script>
    <script type="text/javascript" src="/js/commonAll.js"></script>
</head>
<body>
<form name="editForm" data-url="/orderBill/list.do" action="/orderBill/saveOrUpdate.do" method="post" id="editForm">
    <input type="hidden" name="id" value="${orderBill.id}">
    <div id="container">
        <div id="nav_links">
            <span style="color: #1A5CC6;">订单编辑</span>
            <div id="page_close">
                <a>
                    <img src="/images/common/page_close.png" width="20" height="20" style="vertical-align: text-top;"/>
                </a>
            </div>
        </div>
        <div class="ui_content">
            <table cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                <tr>
                    <td class="ui_text_rt" width="140">订单编码</td>
                    <td class="ui_text_lt">
                        <input name="sn" class="ui_input_txt02" value="${orderBill.sn}"/>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">供应商</td>
                    <td class="ui_text_lt">
                        <select name="supplier.id" class="ui_select01">
                            <c:forEach items="${suppliers}" var="supplier">
                                <option value="${supplier.id}" ${orderBill.supplier.id==supplier.id?'selected':''}>${supplier.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">业务时间</td>
                    <td class="ui_text_lt">
                        <fmt:formatDate value="${orderBill.vdate}" pattern="yyyy-MM-dd HH:mm:ss" var="vdate"></fmt:formatDate>
                        <input name="vdate" class="ui_input_txt02" value="${vdate}" onclick="WdatePicker();"/>
                    </td>
                </tr>

                <tr>
                    <td class="ui_text_rt" width="140">单据明细</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="button" value="添加明细" class="ui_input_btn01 appendRow"/>
                        <table class="edit_table" cellspacing="0" cellpadding="0" border="0" style="width: auto">
                            <thead>
                            <tr>
                                <th width="10"></th>
                                <th width="200">货品</th>
                                <th width="120">品牌</th>
                                <th width="80">价格</th>
                                <th width="80">数量</th>
                                <th width="80">金额小计</th>
                                <th width="150">备注</th>
                                <th width="60"></th>
                            </tr>
                            </thead>
                            <c:if test="${orderBill.id==null}">
                                <tbody id="edit_table_body">
                                <tr>
                                    <td></td>
                                    <td>
                                        <input disabled="true" readonly="true" class="ui_input_txt02" tag="name"/>
                                        <img src="/images/common/search.png" class="searchproduct"/>
                                        <input type="hidden" name="items[0].product.id" tag="pid"/>
                                    </td>
                                    <td><span tag="brand"></span></td>
                                    <td><input tag="costPrice" name="items[0].costPrice"
                                               class="ui_input_txt00"/></td>
                                    <td><input tag="number" name="items[0].number"
                                               class="ui_input_txt00"/></td>
                                    <td><span tag="amount"></span></td>

                                    <td><input tag="remark" name="items[0].remark"
                                               class="ui_input_txt02"/></td>
                                    <td>
                                        <a href="javascript:;" class="removeItem">删除明细</a>
                                    </td>
                                </tr>
                                </tbody>

                            </c:if>
                            <c:if test="${orderBill.id != null}">
                                <tbody id="edit_table_body">
                            <c:forEach items="${orderBill.items}" var="bill">
                                <tr>
                                    <td></td>
                                    <td>
                                        <input disabled="true" readonly="true" class="ui_input_txt02" tag="name" value="${bill.product.name}"/>
                                        <img src="/images/common/search.png" class="searchproduct"/>
                                        <input type="hidden" name="items[0].product.id" tag="pid" value="${bill.product.id}"/>
                                    </td>
                                    <td><span tag="brand">${bill.product.brandName}</span></td>
                                    <td><input tag="costPrice" name="items[0].costPrice" value="${bill.costPrice}"
                                               class="ui_input_txt00"/></td>
                                    <td><input tag="number" name="items[0].number" value="${bill.number}"
                                               class="ui_input_txt00"/></td>
                                    <td><span tag="amount" >${bill.amount}</span></td>

                                    <td><input tag="remark" name="items[0].remark" value="${bill.remark}"
                                               class="ui_input_txt02"/></td>
                                    <td>
                                        <a href="javascript:;" class="removeItem">删除明细</a>
                                    </td>
                                </tr>
                            </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td class="ui_text_lt">
                        &nbsp;<input type="submit" value="确定保存" class="ui_input_btn01"/>
                        &nbsp;<input id="cancelbutton" type="button" value="重置" class="ui_input_btn01"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>
</body>
</html>