<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>信息管理系统</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/js/jquery/plugins/ajax-form/jquery.form.min.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/plugins/iframeTools.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/My97DatePicker/WdatePicker.js"></script>

    <script type="text/javascript">
        <!--放大镜效果-->
        $(function () {
            $(".searchproduct").click(function () {
                var trObject = $(this).closest("tr");
                url = "/product/selectProductList.do";
                $.dialog.open(url, {
                        id: 'evaluate',
                        title: '选择商品列表',
                        width: 900,
                        height: 600,
                        left: '50%',
                        top: '50%',
                        background: '#000000',
                        opacity: 0.1,//透明度
                        lock: true,
                        resize: false,
                        close: function () {

                            var json = $.dialog.data("json");
                            console.log(json);
                            if(json){

                                trObject.find("input[tag='pid']").val(json.id);
                                trObject.find("input[tag='name']").val(json.name);
                                trObject.find("span[tag='brand']").text(json.brandName);
                                trObject.find("input[tag='salePrice']").val(json.salePrice);

                                //$.dialog.prop("json","");
                                $.dialog.data("json","");
                            }

                        }
                    },
                    false);
            })

        })

        $(function () {

            //计算小计
            $("input[tag='salePrice'],input[tag='number']").change(function () {
                var trObject =  $(this).closest("tr");
                var salePrice = parseFloat(trObject.find("input[tag='salePrice']").val()) || 0;
                var number = parseFloat(trObject.find("input[tag='number']").val()) || 0;

                trObject.find("span[tag='amount']").text((salePrice * number).toFixed(2));

            });


        //添加多条明细
            $(".appendRow").click(function () {

                var newTr = $("#edit_table_body tr:first").clone(true);
                //将之前的数据清空
                newTr.find("input[tag='pid']").val("");
                newTr.find("input[tag='name']").val("");
                newTr.find("input[tag='salePrice']").val("");
                newTr.find("input[tag='number']").val("");
                newTr.find("input[tag='remark']").val("");
                newTr.find("span[tag='amount']").text("");
                newTr.find("span[tag='brand']").text("");
                $("#edit_table_body").append(newTr);

              });
            //在提交表单之前修改表单元素的name属性
            $("#editForm").submit(function () {

                $("#edit_table_body tr").each(function (index, item) {

                    $(item).find("input[tag='pid']").prop("name", "items[" + index + "].product.id");
                    $(item).find("input[tag='salePrice']").prop("name", "items[" + index + "].salePrice");
                    $(item).find("input[tag='number']").prop("name", "items[" + index + "].number");
                    $(item).find("input[tag='remark']").prop("name", "items[" + index + "].remark");
                })

            })

            //删除名细
            $(".removeItem").click(function () {
                //1.判断明细框是否只用条,若只有一条则情况数据即可,
                var trObject = $(this).closest("tr");

                var trNum =  $("#edit_table_body tr").size();
               if(trNum == 1){
                   trObject.find("input[tag='pid']").val("");
                   trObject.find("input[tag='name']").val("");
                   trObject.find("input[tag='salePrice']").val("");
                   trObject.find("input[tag='number']").val("");
                   trObject.find("input[tag='remark']").val("");
                   trObject.find("span[tag='amount']").text("");
                   trObject.find("span[tag='brand']").text("");
               }else {

                //2.否则则删除这行
                   trObject.remove();
               }

            });

        })

    </script>
    <script type="text/javascript" src="/js/commonAll.js"></script>
</head>
<body>
<form name="editForm" data-url="/stockOutcomeBill/list.do" action="/stockOutcomeBill/saveOrUpdate.do" method="post" id="editForm">
    <input type="hidden" name="id" value="${stockOutcomeBill.id}">
    <div id="container">
        <div id="nav_links">
            <span style="color: #1A5CC6;">销售出库单编辑</span>
            <div id="page_close">
                <a>
                    <img src="/images/common/page_close.png" width="20" height="20" style="vertical-align: text-top;"/>
                </a>
            </div>
        </div>
        <div class="ui_content">
            <table cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                <tr>
                    <td class="ui_text_rt" width="140">出库单编码</td>
                    <td class="ui_text_lt">
                        <input name="sn"  class="ui_input_txt02" value="${stockOutcomeBill.sn}"/>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">仓库</td>
                    <td class="ui_text_lt">
                        <select name="depot.id" class="ui_select01">
                            <c:forEach items="${depots}" var="depot">
                                <option value="${depot.id}" ${stockOutcomeBill.depot.id==depot.id?'selected':''}>${depot.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">客户</td>
                    <td class="ui_text_lt">
                        <select name="client.id" class="ui_select01">
                            <c:forEach items="${clients}" var="client">
                                <option value="${client.id}" ${stockOutcomeBill.client.id==client.id?'selected':''}>${client.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">业务时间</td>
                    <td class="ui_text_lt">
                        <fmt:formatDate value="${stockOutcomeBill.vdate}" pattern="yyyy-MM-dd HH:mm:ss" var="vdate"></fmt:formatDate>
                        <input name="vdate" class="ui_input_txt02" value="${vdate}" onclick="WdatePicker();"/>
                    </td>
                </tr>

                <tr>
                    <td class="ui_text_rt" width="140">单据明细</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="button" value="添加明细" class="ui_input_btn01 appendRow"/>
                        <table class="edit_table" cellspacing="0" cellpadding="0" border="0" style="width: auto">
                            <thead>
                            <tr>
                                <th width="10"></th>
                                <th width="200">货品</th>
                                <th width="120">品牌</th>
                                <th width="80">价格</th>
                                <th width="80">数量</th>
                                <th width="80">金额小计</th>
                                <th width="150">备注</th>
                                <th width="60"></th>
                            </tr>
                            </thead>
                            <c:if test="${stockOutcomeBill.id==null}">
                                <tbody id="edit_table_body">
                                <tr>
                                    <td></td>
                                    <td>
                                        <input disabled="true" readonly="true" class="ui_input_txt02" tag="name"/>
                                        <img src="/images/common/search.png" class="searchproduct"/>
                                        <input type="hidden" name="items[0].product.id" tag="pid"/>
                                    </td>
                                    <td><span tag="brand"></span></td>
                                    <td><input tag="salePrice" name="items[0].salePrice"
                                               class="ui_input_txt00"/></td>
                                    <td><input tag="number" name="items[0].number"
                                               class="ui_input_txt00"/></td>
                                    <td><span tag="amount"></span></td>

                                    <td><input tag="remark" name="items[0].remark"
                                               class="ui_input_txt02"/></td>
                                    <td>
                                        <a href="javascript:;" class="removeItem">删除明细</a>
                                    </td>
                                </tr>
                                </tbody>

                            </c:if>
                            <c:if test="${stockOutcomeBill.id != null}">
                                <tbody id="edit_table_body">
                            <c:forEach items="${stockOutcomeBill.items}" var="bill">
                                <tr>
                                    <td></td>
                                    <td>
                                        <input disabled="true" readonly="true" class="ui_input_txt02" tag="name" value="${bill.product.name}"/>
                                        <img src="/images/common/search.png" class="searchproduct"/>
                                        <input type="hidden" name="items[0].product.id" tag="pid" value="${bill.product.id}"/>
                                    </td>
                                    <td><span tag="brand">${bill.product.brandName}</span></td>
                                    <td><input tag="salePrice" name="items[0].salePrice" value="${bill.salePrice}"
                                               class="ui_input_txt00"/></td>
                                    <td><input tag="number" name="items[0].number" value="${bill.number}"
                                               class="ui_input_txt00"/></td>
                                    <td><span tag="amount" >${bill.amount}</span></td>

                                    <td><input tag="remark" name="items[0].remark" value="${bill.remark}"
                                               class="ui_input_txt02"/></td>
                                    <td>
                                        <a href="javascript:;" class="removeItem">删除明细</a>
                                    </td>
                                </tr>
                            </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td class="ui_text_lt">
                        &nbsp;<input type="submit" value="确定保存" class="ui_input_btn01"/>
                        &nbsp;<input id="cancelbutton" type="button" value="重置" class="ui_input_btn01"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>
</body>
</html>