<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/js/jquery/plugins/ajax-form/jquery.form.min.js"></script>
    <script type="text/javascript" src="/js/commonAll.js"></script>
    <title>叩丁狼教育PSS（演示版）-部门管理</title>
    <style>
        .alt td {
            background: black !important;
        }
    </style>

</head>
<body>
<form id="searchForm" action="/productStock/list.do" method="post">
    <div id="container">
        <div class="ui_content">
            <div class="ui_text_indent">
                <div id="box_border">
                    <div id="box_top">搜索</div>
                    <div id="box_bottom">
                     货品名称或编号<input name="keywords" value="${qo.keywords}" class="ui_input_txt02"/>
                        所在仓库
                        <select id="depotId" name="depotId" class="ui_select01">
                            <option value="-1">所有仓库</option>
                            <c:forEach items="${depots}" var="d">
                                <option value="${d.id}" ${d.id == qo.depotId?"selected":''}>${d.name}</option>
                            </c:forEach>

                        </select>
                    货品品牌
                        <select  name="brandId" class="ui_select01">
                             <option value="-1">所有品牌</option>
                        <c:forEach items="${brands}" var="b">
                            <option value="${b.id}" ${b.id == qo.brandId?"selected":''}>${b.name}</option>
                             </c:forEach>
                        </select>
                        库存阈值<input name="limitNumber" value="${qo.limitNumber}" class="ui_input_txt02"/>


                        <input type="button" value="查询" class="ui_input_btn01 btn_page"
                              data-page="1"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui_content">
            <div class="ui_tb">
                <table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <th width="30"><input type="checkbox" id="all"/></th>
                        <th>仓库</th>
                        <th>商品编码</th>
                        <th>商品名称</th>
                        <th>品牌</th>
                        <th>库存数量</th>
                        <th>成本</th>
                        <th>库存汇总</th>
                        <th></th>
                    </tr>
                    <tbody>
                    <c:forEach items="${result.listData}" var="ele">
                        <tr>
                            <td><input type="checkbox" name="IDCheck" class="acb"/></td>
                            <td>${ele.depot.name}</td>
                            <td>${ele.product.sn}</td>
                            <td>${ele.product.name}</td>
                            <td>${ele.product.brandName}</td>
                            <td>${ele.storeNumber}</td>
                            <td>${ele.price}</td>
                            <td>${ele.amount}</td>
                            <td>
                                无
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <jsp:include page="/WEB-INF/views/common/common_page.jsp"/>
        </div>
    </div>
</form>
</body>
</html>
