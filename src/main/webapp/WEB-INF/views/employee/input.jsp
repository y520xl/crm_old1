<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>信息管理系统</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/jquery-validate/jquery.validate.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/ajax-form/jquery.form.min.js"></script>
    <script type="text/javascript" src="/js/system/employee.js"></script>
    <script type="text/javascript" src="/js/commonAll.js"></script>
    <script type="text/javascript">

        $(function () {
            $("#editForm").validate({
                rules:{
                    name:{
                        required:true,
                        maxlength:8
                    },
                    password:{
                        required: true,
                        minlength: 5
                    },
                    /*repassword:{
                        required: true,
                        minlength: 5,
                        equalTo: "#password"
                    }*/
                    email: {
                        required: true,
                        email: true
                    },
                    age:"required"
                },
                messages:{
                    name:{
                        required:"用户名不能为空",
                        maxlength:"不能超过8个字符"
                    },
                    password:{
                        required: "密码不能为空",
                        minlength: "至少长度为5"
                    },
                    /*repassword:{
                        required:"验证密码不能为空",
                        minlength: "至少长度为5",
                        equalTo:"请输入和上面一致的密码"
                    }*/
                    email: {
                        required: "邮箱不能为空",
                        email: "请输入一个有效的邮箱地址"
                    },
                    age:"年龄不能为空"
                }

            })

        })

    </script>

</head>
<body>
<form name="editForm" action="/employee/saveOrUpdate.do" method="post" id="editForm">
    <input type="hidden"  name="id" value="${emp.id}">
    <div id="container">
        <div id="nav_links">
            <span style="color: #1A5CC6;">用户编辑</span>
            <div id="page_close">
                <a>
                    <img src="/images/common/page_close.png" width="20" height="20" style="vertical-align: text-top;"/>
                </a>
            </div>
        </div>
        <div class="ui_content">
            <table cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                <tr>
                    <td class="ui_text_rt" width="140">用户名</td>
                    <td class="ui_text_lt">
                        <input name="name" class="ui_input_txt02" value="${emp.name}"/>
                    </td>
                </tr>
                <c:if test="${emp.id==null}">
                <tr>
                    <td class="ui_text_rt" width="140">密码</td>
                    <td class="ui_text_lt">
                        <input type="password" name="password"  class="ui_input_txt02"/>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">验证密码</td>
                    <td class="ui_text_lt">
                        <input name="repassword" type="password" class="ui_input_txt02" />
                    </td>
                </tr>
                </c:if>
                <tr>
                    <td class="ui_text_rt" width="140">Email</td>
                    <td class="ui_text_lt">
                        <input name="email" value="${emp.email}" class="ui_input_txt02"/>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">年龄</td>
                    <td class="ui_text_lt">
                        <input name="age" value="${emp.age}" class="ui_input_txt02"/>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">所属部门</td>
                    <td class="ui_text_lt">
                        <select name="dept.id" class="ui_select01">
                            <c:forEach items="${dept}" var="ele">
                            <option value="${ele.id}" ${emp.dept.id==ele.id?'selected':''}>${ele.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">超级管理员</td>
                    <td class="ui_text_lt">
                        <input type="checkbox" name="admin" ${emp.admin?"checked":""} class="ui_checkbox01"/>

                    </td>
                </tr>
               <tr>
                    <td class="ui_text_rt" width="140">角色</td>
                    <td class="ui_text_lt">
                        <table>
                            <tr>
                                <td>
                                    <select multiple="true" class="ui_multiselect01 allRoles">

                                        <c:forEach items="${roles}" var="ele">
                                            <option value="${ele.id}">${ele.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td align="center">
                                    <input type="button" id="select" value="-->" class="left2right"/><br/>
                                    <input type="button" id="selectAll" value="==>" class="left2right"/><br/>
                                    <input type="button" id="deselect" value="<--" class="left2right"/><br/>
                                    <input type="button" id="deselectAll" value="<==" class="left2right"/>
                                </td>
                                <td>
                                    <select multiple="true" class="ui_multiselect01 selectedRoles" name="roleIds">
                                        <c:forEach items="${emp.roles}" var="ele">
                                            <option value="${ele.id}">${ele.name}</option>
                                        </c:forEach>

                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="ui_text_lt">
                        &nbsp;<input type="submit" value="确定保存" class="ui_input_btn01"/>
                        &nbsp;<input id="cancelbutton" type="button" value="重置" class="ui_input_btn01"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>
</body>
</html>