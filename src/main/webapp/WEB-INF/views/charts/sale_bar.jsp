<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
   <script type="text/javascript" src="/js/echarts/echarts-all.js"></script>

    <title>叩丁狼教育PSS（演示版）-销售报表柱状图</title>


</head>
<body>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="width: 600px;height:400px;"></div>

<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));

    // 指定图表的配置项和数据
    var option = {
        title: {
            text: '销售报表',
            subtext: '${groupBy}',
            x:'center'
        },
        tooltip: {},
        legend: {
            data:['销售总额'],
            x:'left'
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        xAxis: {
            data: ${groupByType}
        },
        yAxis: {},
        series: [{
            name: '销量',
            type: 'bar',
            data: ${totalAmount},
            markPoint : {
                data : [
                    {type : 'max', name: '最大销售总额'},
                    {type : 'min', name: '最小销售总额'}
                ]
            },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }

        }]
    };
    myChart.setOption(option);
</script>

</body>
</html>
