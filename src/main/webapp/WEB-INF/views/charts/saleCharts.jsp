<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/js/jquery/plugins/ajax-form/jquery.form.min.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/plugins/iframeTools.js"></script>
    <script type="text/javascript" src="/js/commonAll.js"></script>
    <title>叩丁狼教育PSS（演示版）-销售报表管理</title>
    <style>
        .alt td{ background:black !important;}
    </style>
    <script type="text/javascript">

            $(function () {

                $(".btn_bar").click(function () {
                    var param = $("#searchForm").serialize();
                    url = "/charts/sale_bar.do?" + param;
                    $.dialog.open(url, {
                        id: 'evaluate',
                        title: '柱状图',
                        width: 600,
                        height: 400,
                        opacity: 0.1,//透明度
                        close: function () {

                        }

                    });

                });
                $(".btn_pie").click(function () {
                    var param = $("#searchForm").serialize();
                    url = "/charts/sale_pie.do?" + param;
                    $.dialog.open(url, {
                        id: 'evaluate',
                        title: '饼状图',
                        width: 600,
                        height: 400,
                        opacity: 0.1,//透明度
                        close: function () {

                        }

                    });

                });

            });
    </script>

</head>
<body>
<form id="searchForm" action="/charts/saleCharts.do" method="post">
    <div id="container">
        <div class="ui_content">
            <div class="ui_text_indent">
                <div id="box_border">
                    <div id="box_top">搜索</div>
                    <div id="box_bottom">
                        <fmt:formatDate value="${qo.beginDate}" pattern="yyyy-MM-dd" var="beginDate"></fmt:formatDate>
                        <fmt:formatDate value="${qo.endDate}" pattern="yyyy-MM-dd" var="endDate"></fmt:formatDate>
                        业务时间
                        <input name="beginDate" onclick="WdatePicker();" value="${beginDate}" class="ui_input_txt02 Wdate beginDate"/>
                        ~
                        <input name="endDate" onclick="WdatePicker();" value="${endDate}" class="ui_input_txt02 Wdate endDate"/>
                        客户
                        <select name="clientId" class="ui_select01">
                            <option value="-1">所有客户</option>
                            <c:forEach items="${clients}" var="c">
                                <option value="${c.id}" ${c.id == qo.clientId?"selected":''}>${c.name}</option>

                            </c:forEach>

                        </select>
                        货品品牌
                        <select id="brandId" name="brandId" class="ui_select01">
                            <option value="-1">所有品牌</option>
                            <c:forEach items="${brands}" var="b">
                                <option value="${b.id}" ${b.id == qo.brandId?"selected":""}>${b.name}</option>
                            </c:forEach>

                        </select>
                        分组
                        <select id="groupByType" name="groupBy" class="ui_select01">
                `           <c:forEach items="${groupByTypes}" var="groupByType">
                                <option value="${groupByType.key}" ${qo.groupBy == groupByType.key?"selected":""}>${groupByType.value}</option>

                            </c:forEach>

                        </select>

                            <input type="button" value="查询" class="ui_input_btn01 btn_page" data-page="1"/>
                    </div>
                            <br/>
                            <input type="button" value="柱状图" class="left2right btn_bar" />
                            <input type="button" value="饼状图" class="left2right btn_pie"/>
                </div>
            </div>
        </div>
        <div class="ui_content">
            <div class="ui_tb">
                <table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <th width="30"><input type="checkbox" id="all"/></th>
                        <th>分组类型</th>
                        <th>销售总数量</th>
                        <th>销售总金额</th>
                        <th>毛利润</th>
                        <th></th>
                    </tr>
                    <tbody>
                    <c:forEach items="${result}" var="ele">
                    <tr>
                        <td><input type="checkbox" name="IDCheck" class="acb" /></td>
                        <td>${ele.groupByType}</td>
                        <td>${ele.totalNumber}</td>
                        <td>${ele.totalAmount}</td>
                        <td>${ele.profit}</td>
                        <td>
                        </td>
                    </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
</body>
</html>
