<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/js/commonAll.js"></script>
    <title>叩丁狼教育PSS（演示版）-权限管理</title>
    <style>
        .alt td{ background:black !important;}
    </style>

    <script type="text/javascript" >
        $(function () {
            $(".btn_loadPermission").click(function () {

                $.dialog({
                    title:"温馨提示",
                    content:"加载权限比较耗时,你确定要加载嘛?",
                    icon:"face-smile",
                    lock:false,
                    ok:function () {
                       var dialog =  $.dialog({
                            title: "温馨提示",
                            icon:"face-smile"
                        })
                        var url = $(".btn_loadPermission").data("url");
                        //发送一次ajax请求去加载权限
                        $.get(url,function (data) {

                            if(data.success){
                                window.location.reload();

                            }
                        })

                    }


                })
            })
        })


    </script>


</head>
<body>
<form id="searchForm" action="/permission/list.do" method="post">
    <div id="container">
        <div class="ui_content">
            <div class="ui_text_indent">
                <div id="box_border">
                    <div id="box_top">搜索</div>
                    <div id="box_bottom">
                        <input type="button" value="加载权限" class="ui_input_btn01 btn_loadPermission" data-url="/permission/reload.do"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui_content">
            <div class="ui_tb">
                <table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <th width="30"><input type="checkbox" id="all"/></th>
                        <th>编号</th>
                        <th>权限名称</th>
                        <th>权限表达式</th>
                        <th></th>
                    </tr>
                    <tbody>
                    <c:forEach items="${result.listData}" var="ele">
                    <tr>
                        <td><input type="checkbox" name="IDCheck" class="acb" /></td>
                        <td>${ele.id}</td>
                        <td>${ele.name}</td>
                        <td>${ele.expression}</td>
                        <td>
                            <a href="javascript:;" data-url="/permission/delete.do?id=${ele.id}" class="btn_delete">删除</a>
                        </td>
                    </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
           <jsp:include page="/WEB-INF/views/common/common_page.jsp"/>
        </div>
    </div>
</form>
</body>
</html>
