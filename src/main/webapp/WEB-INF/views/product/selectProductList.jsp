<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/js/jquery/plugins/ajax-form/jquery.form.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/jquery/plugins/fancybox/jquery.fancybox.css"/>
    <script type="text/javascript" src="/js/jquery/plugins/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/artDialog/artDialog/plugins/iframeTools.js"></script>
    <script type="text/javascript" src="/js/commonAll.js"></script>
    <script type="text/javascript" >

         $(function () {
            $('.fancybox').fancybox();

            $(".btn_selectProduct").click(function () {

                var jsonData = $(this).data("product");
                //共享子窗口中的数据
                $.dialog.data("json",jsonData);
                $.dialog.close();
            })
        });

    </script>
    <title>叩丁狼教育PSS（演示版）-商品管理</title>
    <style>
        .alt td{ background:black !important;}
    </style>

</head>
<body>
<form id="searchForm" action="/product/selectProductList.do" method="post">
    <div id="container">
        <div class="ui_content">
            <div class="ui_text_indent">
                <div id="box_border">
                    <div id="box_top">搜索</div>
                    <div id="box_center">
                        关键字
                        <input type="text" class="ui_input_txt02" name="keywords" value="${qo.keywords}"/>
                        品牌
                        <select class="ui_select01" name="brandId">
                            <option value="-1">--请选择--</option>
                            <c:forEach items="${brands}" var="brand">
                                <option value="${brand.id}" ${qo.brandId == brand.id?"selected":""}>${brand.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div id="box_bottom">
                        <input type="button" value="查询" class="ui_input_btn01 btn_reseacher"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui_content">
            <div class="ui_tb">
                <table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <th width="30"><input type="checkbox" id="all"/></th>
                        <th>编号</th>
                        <th>商品图片</th>
                        <th>商品名称</th>
                        <th>商品编码</th>
                        <th>商品品牌</th>
                        <th>商品成本价</th>
                        <th>商品销售价</th>
                        <th></th>
                    </tr>
                    <tbody>
                    <c:forEach items="${result.listData}" var="ele">
                    <tr>
                        <td><input type="checkbox" name="IDCheck" class="acb" /></td>
                        <td>${ele.id}</td>
                        <td>
                            <a class="fancybox" href="${ele.imagePath}" data-fancybox-group="gallery" title="${ele.name}">
                                <img src="${ele.smallImagePath}" width="60">
                            </a>
                        </td>
                        <td>${ele.name}</td>
                        <td>${ele.sn}</td>
                        <td>${ele.brandName}</td>
                        <td>${ele.costPrice}</td>
                        <td>${ele.salePrice}</td>
                        <td>
                            <input type="button" id="select" data-product='${ele.productData}' value="选择该商品" class="left2right btn_selectProduct"/>
                        </td>
                    </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
           <jsp:include page="/WEB-INF/views/common/common_page.jsp"/>
        </div>
    </div>
</form>
</body>
</html>
